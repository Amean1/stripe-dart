part of '../../../messages.dart';

@JsonSerializable()
class Item {
  /// The ID of the price object.
  final String? price;

  Item({
    this.price,
  });

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);
  Map<String, dynamic> toJson() => _$ItemToJson(this);
}

/// https://stripe.com/docs/api/subscriptions/create?lang=node
@JsonSerializable()
class CreateSubscriptionRequest {
  /// The identifier of the customer to subscribe.
  final String? customer;

  /// A list of up to 20 subscription items, each with an attached price.
  final List<Item>? items;

  CreateSubscriptionRequest({
    this.customer,
    this.items,
  });

  factory CreateSubscriptionRequest.fromJson(Map<String, dynamic> json) =>
      _$CreateSubscriptionRequestFromJson(json);
  Map<String, dynamic> toJson() => _$CreateSubscriptionRequestToJson(this);
}
